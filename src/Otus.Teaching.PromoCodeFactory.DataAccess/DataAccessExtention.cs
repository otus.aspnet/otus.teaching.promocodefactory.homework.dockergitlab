﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public static class DataAccessExtention
    {
        public static IServiceCollection AddDb(this IServiceCollection services, string connectionstring)
        {
            return services.AddDbContext<DataContext>(options => {
                options.UseNpgsql(connectionstring);
               // options.UseLazyLoadingProxies();
            });
        }
    }
}
